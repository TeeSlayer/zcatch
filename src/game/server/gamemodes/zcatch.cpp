/* (c) Magnus Auvinen. See licence.txt in the root of the distribution for more information. */
/* If you are missing that file, acquire a complete release at teeworlds.com.                */
/* zCatch by erd and Teetime                                                                 */

#include <engine/shared/config.h>
#include <game/server/gamecontext.h>
#include <game/server/gamecontroller.h>
#include <game/server/entities/character.h>
#include <game/server/player.h>
#include "zcatch.h"

enum {
	SKINPART_BODY = 0,
	SKINPART_MARKING,
	SKINPART_DECORATION,
	SKINPART_HANDS,
	SKINPART_FEET,
	SKINPART_EYES,
	SKINPART_COUNT
};


CGameController_zCatch::CGameController_zCatch(class CGameContext *pGameServer) :
	IGameController(pGameServer)
{
	m_pGameType = "zCatch";
	m_OldMode = g_Config.m_SvMode;
	m_GameFlags = GAMEFLAG_SURVIVAL;
}

void CGameController_zCatch::Tick()
{
	IGameController::Tick();

	if(m_GameState == IGS_GAME_RUNNING) {
		DoWincheckMatch();
	}

	if(m_OldMode != g_Config.m_SvMode)
	{
		m_OldMode = g_Config.m_SvMode;
	}

	if(Server()->Tick() % Server()->TickSpeed() == 0) {
		for(int i = 0; i < MAX_CLIENTS; i++)
		{
			if(GameServer()->m_apPlayers[i]) {
				RefreshBroadcast(i);
			}
		}
		if(IsWarmup()) {
			for(int i = 0; i < MAX_CLIENTS; i++)
			{
				if(GameServer()->m_apPlayers[i]) {
					GameServer()->m_apPlayers[i]->WarmupReleaseAll();
				}
			}
		}

	}
	for(int i = 0; i < MAX_CLIENTS; i++)
	{
		if(GameServer()->m_apPlayers[i] && GameServer()->m_apPlayers[i]->m_Released &&  GameServer()->m_apPlayers[i]->GetTeam() != TEAM_SPECTATORS) {
			RefreshBroadcast(i);
			CalcPlayerColorFor(GameServer()->m_apPlayers[i]);
			UpdatePlayerColorForAll(GameServer()->m_apPlayers[i]->GetCID(), true);
			GameServer()->m_apPlayers[i]->m_Released = false;
		}
	}
}

void CGameController_zCatch::RefreshBroadcast(int i) {
	CPlayer *TargetPlayer = GameServer()->m_apPlayers[i];
	if(GameServer()->m_apPlayers[i]) {
		if(GetRealPlayerNum() < 3 || !(IsGameRunning() || IsWarmup())) {
			GameServer()->SendBroadcast("", TargetPlayer->GetCID());
			return;
		}
		CPlayer *SourcePlayer = NULL;
		if(TargetPlayer->GetCharacter())
			SourcePlayer = TargetPlayer;
		else if(TargetPlayer->GetSpectatorID() != -1)
			SourcePlayer = GameServer()->m_apPlayers[TargetPlayer->GetSpectatorID()];
		
		if(SourcePlayer && SourcePlayer->GetCharacter()) {
			char aBuf[256];
			int catched = 0;
			for(int j = 0; j < MAX_CLIENTS; j++) {
				if(GameServer()->m_apPlayers[j] && GameServer()->m_apPlayers[j]->m_CaughtBy == SourcePlayer->GetCID())
					catched++;
			}
			if(catched >= GetRealPlayerNum() - 1) {
				GameServer()->SendBroadcast("", TargetPlayer->GetCID());
				return;
			}
			str_format(aBuf, sizeof(aBuf), "%d left", GameServer()->m_pController->GetRealPlayerNum() - 1 - catched);
			GameServer()->SendBroadcast(aBuf, TargetPlayer->GetCID());
		} else {
			GameServer()->SendBroadcast("", TargetPlayer->GetCID());
		}
	}
}

void CGameController_zCatch::DoWincheckRound()
{
	// check for time based win
	if(m_GameInfo.m_TimeLimit > 0 && (Server()->Tick()-m_GameStartTick) >= m_GameInfo.m_TimeLimit*Server()->TickSpeed()*60)
	{
		for(int i = 0; i < MAX_CLIENTS; ++i)
		{
			if(GameServer()->m_apPlayers[i] && GameServer()->m_apPlayers[i]->GetTeam() != TEAM_SPECTATORS &&
				(!GameServer()->m_apPlayers[i]->m_RespawnDisabled || 
				(GameServer()->m_apPlayers[i]->GetCharacter() && GameServer()->m_apPlayers[i]->GetCharacter()->IsAlive())))
				GameServer()->m_apPlayers[i]->m_Score++;
		}

		EndRound();
	}
	else
	{
		// check for survival win
		CPlayer *pAlivePlayer = 0;
		int AlivePlayerCount = 0;
		for(int i = 0; i < MAX_CLIENTS; ++i)
		{
			if(GameServer()->m_apPlayers[i] && GameServer()->m_apPlayers[i]->GetTeam() != TEAM_SPECTATORS &&
				(!GameServer()->m_apPlayers[i]->m_RespawnDisabled || 
				(GameServer()->m_apPlayers[i]->GetCharacter() && GameServer()->m_apPlayers[i]->GetCharacter()->IsAlive())))
			{
				++AlivePlayerCount;
				pAlivePlayer = GameServer()->m_apPlayers[i];
			}
		}

		if(AlivePlayerCount == 0) {		// no winner
			EndRound();
			GameServer()->SendChat(-1, CHAT_ALL, 0, "Everybody lost. There is no winner!");
		}
		else if(AlivePlayerCount == 1)	// 1 winner
		{
			pAlivePlayer->m_Score += g_Config.m_SvBonus;
			EndRound();
			char aBuf[256];
			str_format(aBuf, sizeof(aBuf), "'%s' won the round!", Server()->ClientName(pAlivePlayer->GetCID()));
			GameServer()->SendChat(-1, CHAT_ALL, 0, aBuf);
		}
	}
}

int CGameController_zCatch::OnCharacterDeath(class CCharacter *pVictim, class CPlayer *pKiller, int WeaponID)
{
	int VictimID = pVictim->GetPlayer()->GetCID();

	if(!pKiller || WeaponID == WEAPON_GAME) {
		pVictim->GetPlayer()->HandleLeave();
	}
	else if(pKiller == pVictim->GetPlayer())
		pVictim->GetPlayer()->HandleSuicide();
	else {
		pKiller->Catch(pVictim->GetPlayer());
		CalcPlayerColorFor(pKiller);
		UpdatePlayerColorForAll(pKiller->GetCID(), true);
	}

	if(WeaponID == WEAPON_SELF || WeaponID == WEAPON_GAME)
		pVictim->GetPlayer()->m_RespawnTick = Server()->Tick()+Server()->TickSpeed()*3.0f;

	pVictim->GetPlayer()->ReleaseVictims(pKiller);

	// Update colours
	//OnPlayerInfoChange(pVictim->GetPlayer());

	// update spectator modes for dead players in survival
	//if(m_GameFlags&GAMEFLAG_SURVIVAL)
	//{
	//	for(int i = 0; i < MAX_CLIENTS; ++i)
	//		if(GameServer()->m_apPlayers[i] && GameServer()->m_apPlayers[i]->m_DeadSpecMode)
	//			GameServer()->m_apPlayers[i]->UpdateDeadSpecMode();
	//}
	RefreshBroadcast(pKiller->GetCID());
	RefreshBroadcast(pVictim->GetPlayer()->GetCID());
	return 0;
}
void CGameController_zCatch::ResetValues()
{
	for(int i = 0; i < MAX_CLIENTS; i++)
	{
		if(GameServer()->m_apPlayers[i])
		{
			GameServer()->m_apPlayers[i]->m_CaughtBy = CPlayer::ZCATCH_NOT_CAUGHT;
			GameServer()->m_apPlayers[i]->m_Kills = 0;
			GameServer()->m_apPlayers[i]->m_Deaths = 0;
			GameServer()->m_apPlayers[i]->m_TicksSpec = 0;
			GameServer()->m_apPlayers[i]->m_TicksIngame = 0;
		}
	}
}

void CGameController_zCatch::OnCharacterSpawn(class CCharacter *pChr)
{
	// char aBuf[256];
	// str_format(aBuf, sizeof(aBuf), "\"%s\" SPAWNS!", Server()->ClientName(pChr->GetPlayer()->GetCID()));
	// GameServer()->SendChat(-1, CHAT_ALL, 0, aBuf);
	// default health and armor
	pChr->IncreaseHealth(10);
	if(g_Config.m_SvMode == 2)
		pChr->IncreaseArmor(10);

	// give default weapons
	switch(g_Config.m_SvMode)
	{
		case 1: /* Instagib - Only Riffle */
			pChr->GiveWeapon(WEAPON_LASER, -1);
			break;
		case 2: /* All Weapons */
			pChr->GiveWeapon(WEAPON_HAMMER, -1);
			pChr->GiveWeapon(WEAPON_GUN, g_Config.m_SvWeaponsAmmo);
			pChr->GiveWeapon(WEAPON_GRENADE, g_Config.m_SvWeaponsAmmo);
			pChr->GiveWeapon(WEAPON_SHOTGUN, g_Config.m_SvWeaponsAmmo);
			pChr->GiveWeapon(WEAPON_LASER, g_Config.m_SvWeaponsAmmo);
			break;
		case 3: /* Hammer */
			pChr->GiveWeapon(WEAPON_HAMMER, -1);
			break;
		case 4: /* Grenade */
			pChr->GiveWeapon(WEAPON_GRENADE, g_Config.m_SvGrenadeEndlessAmmo ? -1 : g_Config.m_SvWeaponsAmmo);
			break;
		case 5: /* Ninja */
			pChr->GiveNinja();
			break;
	}

	pChr->GetPlayer()->m_RespawnDisabled = true;

	//Update colour of spawning tees
	pChr->GetPlayer()->m_color = 161;
	UpdatePlayerColorForAll(pChr->GetPlayer()->GetCID(), true);

	RefreshBroadcast(pChr->GetPlayer()->GetCID());

	for(int i = 0; i < MAX_CLIENTS; i++) {
		if(GameServer()->m_apPlayers[i] && pChr->GetPlayer()->GetCID() != i && GameServer()->m_apPlayers[i]->m_CaughtBy == pChr->GetPlayer()->GetCID()) {
			GameServer()->m_apPlayers[i]->Release();
		}
	}
}

void CGameController_zCatch::EndRound()
{
	IGameController::EndRound();

	for(int i = 0; i < MAX_CLIENTS; i++)
	{
		if(GameServer()->m_apPlayers[i])
		{
			if(GameServer()->m_apPlayers[i]->GetTeam() != TEAM_SPECTATORS)
			{
				char aBuf[128];
				str_format(aBuf, sizeof(aBuf), "Kills: %d | Deaths: %d", GameServer()->m_apPlayers[i]->m_Kills, GameServer()->m_apPlayers[i]->m_Deaths);
				GameServer()->SendChat(-1, CHAT_NONE, i, aBuf);

				if(GameServer()->m_apPlayers[i]->m_TicksSpec != 0 || GameServer()->m_apPlayers[i]->m_TicksIngame != 0)
				{
					double TimeInSpec = (GameServer()->m_apPlayers[i]->m_TicksSpec * 100.0) / (GameServer()->m_apPlayers[i]->m_TicksIngame + GameServer()->m_apPlayers[i]->m_TicksSpec);
					str_format(aBuf, sizeof(aBuf), "Spec: %.2f%% | Ingame: %.2f%%", (double) TimeInSpec, (double) (100.0 - TimeInSpec));
					GameServer()->SendChat(-1, CHAT_NONE, i, aBuf);
				}
				GameServer()->m_apPlayers[i]->m_CaughtBy = CPlayer::ZCATCH_NOT_CAUGHT; //Set all players in server as non-caught
				GameServer()->m_apPlayers[i]->m_RespawnDisabled = false;
			}
		}
	}

	if(IsWarmup()) // game can't end when we are running warmup
		return;

	// GameServer()->m_World.m_Paused = true;
	m_SuddenDeath = 0;
	ResetValues();
}

bool CGameController_zCatch::CanChangeTeam(CPlayer *pPlayer, int JoinTeam)
{
	if(pPlayer->m_CaughtBy >= 0)
		return false;
	return true;
}

bool CGameController_zCatch::OnEntity(int Index, vec2 Pos)
{
	if(Index == ENTITY_SPAWN)
		m_aaSpawnPoints[0][m_aNumSpawnPoints[0]++] = Pos;
	else if(Index == ENTITY_SPAWN_RED)
		m_aaSpawnPoints[1][m_aNumSpawnPoints[1]++] = Pos;
	else if(Index == ENTITY_SPAWN_BLUE)
		m_aaSpawnPoints[2][m_aNumSpawnPoints[2]++] = Pos;

	return false;
}

//inline int PackColor(i32 hue, i32 sat, i32 lgt, i32 alpha = 255)
//{
//	return ((alpha << 24) + (hue << 16) + (sat << 8) + lgt);
//}

void CGameController_zCatch::CalcPlayerColorFor(class CPlayer *pP)
{
	int Num = 161;
	for(int i = 0; i < MAX_CLIENTS; i++) {
		if(GameServer()->m_apPlayers[i] && GameServer()->m_apPlayers[i]->m_CaughtBy == pP->GetCID()) {
			Num -= 10;
		}
	}
	if (Num < 0)
		Num = 0;
	pP->m_color = Num;
}

void CGameController_zCatch::UpdatePlayerColorForAll(i32 PlayerCID, bool silent)
{
	CPlayer** apPlayers = GameServer()->m_apPlayers;
	for(i32 CID = 0; CID < MAX_CLIENTS; CID++) {
		if(PlayerCID == CID || !apPlayers[CID] || apPlayers[PlayerCID]->GetTeam() == TEAM_SPECTATORS) continue;
		SendPlayerColor(PlayerCID, CID, silent);
	}
}

void CGameController_zCatch::UpdatePlayerColorForOne(i32 PlayerCID, bool silent)
{
	CPlayer** apPlayers = GameServer()->m_apPlayers;
	for(i32 CID = 0; CID < MAX_CLIENTS; CID++) {
		if(PlayerCID == CID || !apPlayers[CID] || apPlayers[CID]->GetTeam() == TEAM_SPECTATORS) continue;
		SendPlayerColor(CID, PlayerCID, silent);
	}
}


void CGameController_zCatch::SendPlayerColor(i32 FromCID, i32 ToCID, bool silent)
{
	CPlayer** apPlayers = GameServer()->m_apPlayers;
	// send drop first
	CNetMsg_Sv_ClientDrop Msg;
	Msg.m_ClientID = FromCID;
	Msg.m_pReason = "";
	Msg.m_Silent = 1;
	Server()->SendPackMsg(&Msg, MSGFLAG_VITAL|MSGFLAG_NORECORD, ToCID);

	// then update
	CNetMsg_Sv_ClientInfo nci;
	nci.m_ClientID = FromCID;
	nci.m_Local = 0;
	nci.m_Team = 0;
	nci.m_pName = Server()->ClientName(FromCID);
	nci.m_pClan = Server()->ClientClan(FromCID);
	nci.m_Country = Server()->ClientCountry(FromCID);
	nci.m_Silent = silent;

	for(i32 i = 0; i < SKINPART_COUNT; i++)
	{
		nci.m_apSkinPartNames[i] = apPlayers[FromCID]->m_TeeInfos.m_aaSkinPartNames[i];
	}

	for(i32 i = 0; i < SKINPART_COUNT; i++)
	{
		nci.m_aUseCustomColors[i] = 1;
		nci.m_aSkinPartColors[i] = apPlayers[FromCID]->m_color * 0x010000 + 0xff23;
	}

	Server()->SendPackMsg(&nci, MSGFLAG_VITAL|MSGFLAG_NORECORD, ToCID);
}

void CGameController_zCatch::OnPlayerConnect(CPlayer *pPlayer)
{
	IGameController::OnPlayerConnect(pPlayer);
	UpdatePlayerColorForOne(pPlayer->GetCID(), true);
	UpdatePlayerColorForAll(pPlayer->GetCID(), false);
	if(pPlayer->m_CaughtBy != -1 && GameServer()->m_apPlayers[pPlayer->m_CaughtBy]) {
		RefreshBroadcast(GameServer()->m_apPlayers[pPlayer->m_CaughtBy]->GetCID());
		CalcPlayerColorFor(GameServer()->m_apPlayers[pPlayer->m_CaughtBy]);
		UpdatePlayerColorForAll(GameServer()->m_apPlayers[pPlayer->m_CaughtBy]->GetCID(), true);
	}
}

void CGameController_zCatch::OnPlayerDisconnect(CPlayer *pPlayer)
{
	if(pPlayer->m_CaughtBy != -1 && GameServer()->m_apPlayers[pPlayer->m_CaughtBy]) {
		int c = GameServer()->m_apPlayers[pPlayer->m_CaughtBy]->GetCID();
		pPlayer->HandleLeave();
		RefreshBroadcast(GameServer()->m_apPlayers[c]->GetCID());
		CalcPlayerColorFor(GameServer()->m_apPlayers[c]);
		UpdatePlayerColorForAll(GameServer()->m_apPlayers[c]->GetCID(), true);
	}

	IGameController::OnPlayerDisconnect(pPlayer);
}

void CGameController_zCatch::OnSpec(CPlayer *pPlayer)
{
	UpdatePlayerColorForOne(pPlayer->GetCID(), true);
	UpdatePlayerColorForAll(pPlayer->GetCID(), true);
	if(pPlayer->m_CaughtBy != -1 && GameServer()->m_apPlayers[pPlayer->m_CaughtBy]) {
		int c = GameServer()->m_apPlayers[pPlayer->m_CaughtBy]->GetCID();
		pPlayer->HandleLeave();
		RefreshBroadcast(GameServer()->m_apPlayers[c]->GetCID());
		CalcPlayerColorFor(GameServer()->m_apPlayers[c]);
		UpdatePlayerColorForAll(GameServer()->m_apPlayers[c]->GetCID(), true);
	}
}

void CGameController_zCatch::OnUnSpec(CPlayer *pPlayer)
{
	if(pPlayer->m_CaughtBy != -1 && GameServer()->m_apPlayers[pPlayer->m_CaughtBy]) {
		int c = GameServer()->m_apPlayers[pPlayer->m_CaughtBy]->GetCID();
		RefreshBroadcast(GameServer()->m_apPlayers[c]->GetCID());
		CalcPlayerColorFor(GameServer()->m_apPlayers[c]);
		UpdatePlayerColorForAll(GameServer()->m_apPlayers[c]->GetCID(), true);
	}
}
