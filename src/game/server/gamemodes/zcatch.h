/* (c) Magnus Auvinen. See licence.txt in the root of the distribution for more information. */
/* If you are missing that file, acquire a complete release at teeworlds.com.                */
/* zCatch by erd and Teetime                                                                 */

#ifndef GAME_SERVER_GAMEMODES_ZCATCH_H
#define GAME_SERVER_GAMEMODES_ZCATCH_H

#include <game/server/gamecontroller.h>
typedef int32_t i32;

class CGameController_zCatch: public IGameController
{
	int m_OldMode;

public:
	CGameController_zCatch(class CGameContext *pGameServer);
	virtual void Tick();
	virtual void DoWincheckRound();

	void ResetValues();
	//virtual void StartRound();
	virtual void OnCharacterSpawn(class CCharacter *pChr);
	virtual int OnCharacterDeath(class CCharacter *pVictim, class CPlayer *pKiller, int WeaponID);
	virtual bool OnEntity(int Index, vec2 Pos);
	virtual bool CanChangeTeam(CPlayer *pPlayer, int JoinTeam);
	virtual void EndRound();
	virtual void OnPlayerConnect(class CPlayer *pPlayer);
	virtual void OnPlayerDisconnect(class CPlayer *pPlayer);

	void RefreshBroadcast(int);
	void CalcPlayerColor();

	void CalcPlayerColorFor(class CPlayer *pP);
	void UpdatePlayerColorForAll(i32 PlayerCID, bool);
	void UpdatePlayerColorForOne(i32 PlayerCID, bool);
	void SendPlayerColor(i32 FromCID, i32 ToCID, bool);

	virtual void OnSpec(CPlayer *pPlayer);
	virtual void OnUnSpec(CPlayer *pPlayer);
};

#endif
